# Mumla

Mumla is a fork and continuation of [Plumble](https://github.com/acomminos/Plumble),
a robust GPLv3 Mumble client for Android originally written by Andrew Comminos.
It uses the [Humla](https://gitlab.com/quite/humla) protocol implementation
(forked from Comminos' [Jumble](https://github.com/acomminos/Jumble)).

Mumla should run on Android 4.0 (IceCreamSandwich) and later (API Level 14+).

More to come...

## Download

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/packages/se.lublin.mumla/)

## Building on GNU/Linux

    git submodule update --init --recursive
    ./gradlew assembleDebug

It's that simple!

## License

Mumla's [LICENSE](LICENSE) is GNU GPL v3+.
